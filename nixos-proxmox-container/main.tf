#resource "proxmox_virtual_environment_file" "configuration_nix" {
#  content_type = "snippets"
#  datastore_id = "local"
#  node_name = "proxform"
#  file_name = "${}"
#  source_raw {
#    data = <<EOF
#{ config, pkgs, ... }:
#
#{
#  imports = [ <nixpkgs/nixos/modules/virtualisation/lxc-container.nix> ];
#
#  environment.variables = {
#    HISTFILESIZE = "";
#    HISTSIZE = "";
#    HISTTIMEFORMAT = "%F %T ";
#    NIX_SSL_CERT_FILE = "/etc/ssl/certs/ca-certificates.crt";
#  };
#
#  systemd.mounts = [{
#    where = "/sys/kernel/debug";
#    enable = false;
#  }];
#
#  environment.systemPackages = with pkgs; [
#    vim
#    binutils
#  ];
#}
#EOF
#  }
#}

output "nixos_containers" {
  value = proxmox_virtual_environment_container.nixos_container
}

resource "proxmox_virtual_environment_container" "nixos_container" {
  description = "Some long convoluted description with variables"
  node_name = "${var.node_name}"
  count = "${var.container_count}"

  initialization {
    # TODO: find these vars - gunna need a randomized number at the end of it
    hostname = "${var.app_name}-${var.env_name}-deploy${var.deploy_version_number}-${element(random_id.unique_hostname_suffix.*.hex, count.index)}"

    ip_config {
      ipv4 {
        address = "dhcp"
      }
    }

    user_account {
      password = random_password.container_password.result
    }
  }

  network_interface {
    name = "eth0"
  }

  operating_system {
    template_file_id = "${var.template_file_id}"
    type             = "nixos"
  }

  #cmode = "console"
  #console {
  #  # This is set as suggested by the nixos docs.
  #  mode = "console"
  #}

  unprivileged = true

  features {
    nesting = true
  }

  memory {
    dedicated = "${var.memory_dedicated}"
  }

  cpu {
    cores = "${var.cpu_cores}"
  }

# Not sure if we need these for secrets/config. I'll find out later.
#
#  mount_point {
#    # bind mount, *requires* root@pam authentication
#    volume = "/mnt/bindmounts/shared"
#    path   = "/mnt/shared"
#  }
#
#  mount_point {
#    # volume mount, a new volume will be created by PVE
#    volume = "local-lvm"
#    size   = "10G"
#    path   = "/mnt/volume"
#  }

# I think these can stay the default, but I'll want it to start on creation as well as autostart
#
#  startup {
#    order      = "3"
#    up_delay   = "60"
#    down_delay = "60"
#  }

}

# I don't think I'll need this. For the time being, it should be easy enough to download the image manually.
# Either that, or I'll end up setting up a custom one myself.
#
# resource "proxmox_virtual_environment_download_file" "nixos_lxc_img" {
#   content_type = "vztmpl"
#   datastore_id = "local"
#   # TODO: get this set up - probably in the deploy setup per service. Like my services will be on
#   # specific hosts. Of course, if I implement a compute cluster, then I might have to change this later.
#   node_name    = "${current_node}"
#   url          = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.tar.gz"
# }

resource "random_password" "container_password" {
  length           = 16
  special          = false
}

resource "random_id" "unique_hostname_suffix" {
  count = "${var.container_count}"
  byte_length =4
}
