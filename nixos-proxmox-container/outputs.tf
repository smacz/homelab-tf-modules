output "container_password" {
  value     = random_password.container_password.result
  sensitive = true
}

output "random" {
  value = ["${random_id.unique_hostname_suffix.*.hex}"]
}
