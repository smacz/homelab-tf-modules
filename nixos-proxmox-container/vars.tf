variable "node_name" {
  type = string
}

variable "app_name" {
  type = string
}

variable "env_name" {
  type = string
}

variable "deploy_version_number" {
  type = string
}

variable "template_file_id" {
  type = string
}

variable "container_count" {
  type = number
}

variable "cpu_cores" {
  type = number
}

variable "memory_dedicated" {
  type = number
}

variable "nixos_app_config" {
  type = string
}
